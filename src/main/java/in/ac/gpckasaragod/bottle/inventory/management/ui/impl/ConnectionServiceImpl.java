/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.bottle.inventory.management.ui.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author student
 */
public class ConnectionServiceImpl {
        
  String jdbcUrl = "jdbc:mysql://localhost:3306/";
  String databaseName = "BOTTLE_INVENTORY_MANAGEMENT";
  String connectionString = jdbcUrl+databaseName;
  String username = "Root";
  String password="mysql";
  public Connection getConnection() throws SQLException{
  return DriverManager.getConnection(connectionString , username , password); 
   
   
  }    
}
