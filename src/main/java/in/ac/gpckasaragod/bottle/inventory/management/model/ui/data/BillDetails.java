/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.bottle.inventory.management.model.ui.data;

import java.util.Date;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class BillDetails {
    private Integer id;
    private Integer itemid;
    private String customerName;
    private String customerphone;
    private Date billDate;

    public BillDetails(Integer id, Integer itemid, String customerName, String customerphone, Date billDate) {
        this.id = id;
        this.itemid = itemid;
        this.customerName = customerName;
        this.customerphone = customerphone;
        this.billDate = billDate;
    }
    private static final Logger LOG = Logger.getLogger(BillDetails.class.getName());

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getItemid() {
        return itemid;
    }

    public void setItemid(Integer itemid) {
        this.itemid = itemid;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerphone() {
        return customerphone;
    }

    public void setCustomerphone(String customerphone) {
        this.customerphone = customerphone;
    }

    public Date getBillDate() {
        return billDate;
    }

    public void setBillDate(Date billDate) {
        this.billDate = billDate;
    }
   
 
    
    
}
