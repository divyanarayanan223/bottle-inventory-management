/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.bottle.inventory.management.service;

import java.util.List;



/**
 *
 * @author student
 */
public interface DetailService 
{
   public String saveBillItem(Integer id,Integer ItemId,String ConsumerName,String ConsumerPhone,java.util.Date BillDate,float price);
   public DetailService readBillItem(Integer id);
   public List<Item> getAllBillItems();
   public String updateBillItems(Integer id,Integer ItemId,String ConsumerName,String ConsumerPhone,java.util.Date BillDate,float price);
   public String deleteBillItems(Integer id);
   
}
