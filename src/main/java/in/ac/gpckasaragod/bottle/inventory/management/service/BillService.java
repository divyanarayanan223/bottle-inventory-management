/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.bottle.inventory.management.service;

import java.awt.List;

/**
 *
 * @author student
 */
public interface BillService {
    public String saveBillService(Integer itemid,String customername,String consumerphone,java.util.Date billdate,Float price);
    public BillDetails readBillDetails(Integer id);
    public List<BillDetails> getAllBillDetails();
    public String updateBillDetails(Integer id,Integer itemid,String customername,String consumerphone,java.util.Date billdate,Float price);
    public String deleteBillDetails(Integer id);
}
