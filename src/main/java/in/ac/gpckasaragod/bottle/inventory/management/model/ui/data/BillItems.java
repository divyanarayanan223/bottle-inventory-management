/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.bottle.inventory.management.model.ui.data;

import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class BillItems {
    private Integer id;
    private Integer itemid;
    private Integer billid;
    private Integer quantity;
    private Float price;
    private Float totalprice;

    public BillItems(Integer id, Integer itemid, Integer billid, Integer quantity, Float price, Float totalprice) {
        this.id = id;
        this.itemid = itemid;
        this.billid = billid;
        this.quantity = quantity;
        this.price = price;
        this.totalprice = totalprice;
    }
    private static final Logger LOG = Logger.getLogger(BillItems.class.getName());

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getItemid() {
        return itemid;
    }

    public void setItemid(Integer itemid) {
        this.itemid = itemid;
    }

    public Integer getBillid() {
        return billid;
    }

    public void setBillid(Integer billid) {
        this.billid = billid;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Float getTotalprice() {
        return totalprice;
    }

    public void setTotalprice(Float totalprice) {
        this.totalprice = totalprice;
    }
}
