/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.bottle.inventory.management.model.ui.data;

import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class ItemList {
    private Integer itemid;
    private String itemName;
    private Integer quantity;
    private String description;
    private Float price;
    private Integer id;

    public ItemList(Integer itemid, String itemName, Integer quantity, String description, Float price, Integer id) {
        this.itemid = itemid;
        this.itemName = itemName;
        this.quantity = quantity;
        this.description = description;
        this.price = price;
        this.id = id;
    }
    private static final Logger LOG = Logger.getLogger(ItemList.class.getName());

    public Integer getItemid() {
        return itemid;
    }

    public void setItemid(Integer itemid) {
        this.itemid = itemid;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
     
}