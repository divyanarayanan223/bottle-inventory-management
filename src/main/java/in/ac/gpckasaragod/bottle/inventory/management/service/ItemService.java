/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.bottle.inventory.management.service;

import java.awt.List;

/**
 *
 * @author student
 */
public interface ItemService {
          public String saveITEM_LIST(String itemname,Integer quantity,String description,Float price);
          public  ITEM_LIST readITEM_LIST(Integer id);
          public  List<ITEM_LIST> getAllITEM_LISTS();
          public String updateITEM_LIST(Integer id,String itemname,Integer quantity,String description,Float price);
          public String deleteITEM_LIST(Integer id);
          
          
}
